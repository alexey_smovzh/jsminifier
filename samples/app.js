/*
    Multiline comment

    to run Python http server type:

    $ python3 -m http.server --directory jsminifier/samples/

*/
import Car from './lib/class.js'
import * as mp from './lib/module.js';

/** to display tests results */
let table;

// Array
const numbers = [45, 4, 9, 16, 25];

function displayResult(test, result) {
    table.insertRow().outerHTML="<td width='200'>"+test+"</td><td>"+result+"</td>";
}

// single line comment // with check on additional slashes

/*
    test import functions from another file
*/
function testImport() {
    try {
        if (mp.isSum() == true && mp.isMultiply() == true) {
            displayResult('import', 'pass')
        } else {
            throw "wrong result"
        } 
    } catch(err) {
        displayResult('import', 'fail')
    }
}
// Test Class
function testClass() {
    try {
        var car = new Car();
        if (car.age() == true) {
            displayResult('Class', 'pass')
        }
    } catch(err) {
        displayResult('Class', 'fail')
    }
}
// Test loop
let total = 0;
function sum(value, index, array) {
    total += value
}
function testForEachLoop() {
    try {
        numbers.forEach(sum)
        if (total == 99) {
            displayResult('ForEach loop', 'pass')
        } else {
            throw "wrong result"
        }
    } catch(err) {
        displayResult('ForEach loop', 'fail')
    }
}
// Another For loop
function testForLoop(language) {
    try {
        let result = "";
        for (let x of language) {
            result += x;
        }
        if (result == language) {
            displayResult('For loop', 'pass')
        } 
    } catch(err) {
        displayResult('ForEach loop', 'fail')
    }
}
// Test switch
function testSwitch() {
    var number = 20;
    switch(number) {
        case 49: 
            break;
        case 4:
            break;
        case 16: 
            break;
        default: 
            displayResult('Switch', 'pass')
    }
}
// Test try..catch
function testTryCatch() {
    try {
        allll('test')
    } catch (err) {
        displayResult('Try..Catch', 'pass')
    }
}
// Test while loop
function testWhileLoop(start, end) {
    try {
        while (start < end) {
            start++
        }
        do {
            start--
        } while (start > 0)
        if (start != end) {
            displayResult('While loop', 'pass')
        } 
    } catch(err) {
        displayResult('While loop', 'fail')
    }
}
// Test regexp
function testRegexp(){
    try {
        const p = 12;
        let text = "Hello, from regexp test!"
        let position = text.search(/\breg/);   
        if (position == p
            && /z/.test(text) == false) {            
            displayResult('Regexp', 'pass')    
        }
    } catch(err) {
        displayResult('Regexp', 'fail')
    }
}
// Test Async..Await
function asyncResponse(pause) {
    return new Promise(resolve=> {
        setTimeout(() => {
            resolve(true);
        }, pause)
    })
}
async function testAsyncAwait() {
    try {
        const response = await asyncResponse(1000);
        if (response == true) {
            displayResult('Async..Await', 'pass')    
        }
    } catch(err) {
        displayResult('Async..Await', 'fail')    
    }
}
/* 
/* 
    and another multiline comment inside another comment
    // test for slashes
*/
function runTests() {
    table = document.getElementById("table");

    testImport()
    testClass()
    testForEachLoop()
    testForLoop('JavaScript')
    testSwitch()
    testTryCatch()
    testWhileLoop(5, 12)
    testRegexp()
    testAsyncAwait();

}

window.onload = runTests;


