/**
 * Sample Class
 */
export default class Car {
    // constructor
    constructor(name, year) {
        this.name = 'car';
        this.year = '2000';
    }
    // Calculate age
    age() {
        let date = new Date();
        if (date.getFullYear() > this.year) {
            return true
        }
    }

}