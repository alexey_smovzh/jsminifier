import { makeSum } from './module2.js';
import makeMultiply from './module3.js';

// Exported function
export function isSum() {
    if (makeSum(2, 2) == 4) {
        return true
    } else {
        return false
    }
}

// Exported function
export function isMultiply() {            
    if(makeMultiply(2,2) == 4) {
        return true
    } else {
        return false
    }
}
