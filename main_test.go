package main

import "testing"

// Test newName(..)
func TestNewName(t *testing.T) {
	input := map[int][]byte{
		7:     []byte("g"),
		36:    []byte("J"),
		76:    []byte("xa"),
		1698:  []byte("HF"),
		5394:  []byte("LYa"),
		321:   []byte("aif"),  // 'if' keyword
		48746: []byte("avar"), // 'var' keyword
	}

	for k, v := range input {
		result := newName(k)
		if compare(result, &v) == false {
			t.Fatal("newName() for number:", k, "return wrong result", result)
		}
	}
}

// Test compare(..)
func TestCompare(t *testing.T) {
	input := []struct {
		i []byte
		j []byte
	}{
		{[]byte("await"), []byte{97, 119, 97, 105, 116}},
		{[]byte{10, 35, 76, 17, 70, 120}, []byte{10, 35, 40, 17, 70, 120}},
	}

	for i, v := range input {
		result := compare(&v.i, &v.j)
		// Negative
		if result != true && i == 0 {
			t.Fatal("Compare() return false on the equals arrays")
		}
		// False positive
		if result != false && i == 1 {
			t.Fatal("Compare() return true on the different arrays")
		}
	}
}

// Test findEndMultilineComment(..)
func TestFindEndMultilineComment(t *testing.T) {
	line := []byte("multi \n line \n comment */")
	result := findEndMultilineComment(&line, 12)
	if result != 24 {
		t.Fatal("findEndMultilineComment return wrong position\n Expected: 24  return: ", result)
	}
}

// Test findEndSinglelineComment(..)
func TestFindEndSinglelineComment(t *testing.T) {
	line := []byte("// single line comment\n")
	line_win := []byte("// windows comment\r\n")

	result := findEndSinglelineComment(&line, 6)
	if result != 22 {
		t.Fatal("findEndSinglelineComment() return wrong position\n Expected: 23\nReceived: ", result)
	}
	result = findEndSinglelineComment(&line_win, 6)
	if result != 19 {
		t.Fatal("findEndSinglelineComment() return wrong position for CRLF end of string\n Expected: 19\nReceived: ", result)
	}
}

// Test findEndString (..)
func TestFindEndString(t *testing.T) {
	line := []byte("const some = 'http://www.some.com'; const next = 0;")
	len := len(line)

	for i := 0; i < len; i++ {
		if line[i] == APOSTROPHE {
			result := findEndString(&line, i, APOSTROPHE)
			i = result + 1
			if result != 33 {
				t.Fatal("findEndString() return wrong position\n Expected: 33  return: ", result)
			}
		}
	}
}

// Test isBreaker (..)
func TestIsBreaker(t *testing.T) {
	positive := []byte{40, 41, 123, 125, 46, 44, 61, 58, 59, 47, 92, 43, 45, 42, 60, 62, 34, 39, 9, 32, 10, 13}
	negative := []byte{66, 75, 90, 97, 109, 122}

	for _, b := range positive {
		if isBreaker(b) == false {
			t.Fatal("isBreaker() positive test failed on symbol: ", b)
		}
	}
	for _, b := range negative {
		if isBreaker(b) == true {
			t.Fatal("isBreaker() negative test failed on symbol: ", b)
		}
	}
}

// Test parse (..)
func TestParse(t *testing.T) {
	input := []byte(`/*
						Multiline comment
 					*/
					async function getLogin(username, password) {
						// Single lin comment
						const payload = JSON.stringify({'username' :username});
						var token = await postRequest('http://localhost:8080/login', payload);
					}`)

	lexems := []*Lexem{
		{[]byte("async "), ASYNC},
		{[]byte("function "), FUNCTION},
		{[]byte("getLogin"), UNKNOWN},
		{[]byte("("), UNKNOWN},
		{[]byte("username"), UNKNOWN},
		{[]byte(","), UNKNOWN},
		{[]byte("password"), UNKNOWN},
		{[]byte(")"), UNKNOWN},
		{[]byte("{"), UNKNOWN},
		{[]byte("const "), CONST},
		{[]byte("payload"), UNKNOWN},
		{[]byte("="), UNKNOWN},
		{[]byte("JSON"), UNKNOWN},
		{[]byte("."), UNKNOWN},
		{[]byte("stringify"), UNKNOWN},
		{[]byte("("), UNKNOWN},
		{[]byte("{"), UNKNOWN},
		{[]byte("'username'"), STRING},
		{[]byte(":"), UNKNOWN},
		{[]byte("username"), UNKNOWN},
		{[]byte("}"), UNKNOWN},
		{[]byte(")"), UNKNOWN},
		{[]byte(";"), UNKNOWN},
		{[]byte("var "), VAR},
		{[]byte("token"), UNKNOWN},
		{[]byte("="), UNKNOWN},
		{[]byte("await "), AWAIT},
		{[]byte("postRequest"), UNKNOWN},
		{[]byte("("), UNKNOWN},
		{[]byte("'http://localhost:8080/login'"), STRING},
		{[]byte(","), UNKNOWN},
		{[]byte("payload"), UNKNOWN},
		{[]byte(")"), UNKNOWN},
		{[]byte(";"), UNKNOWN},
		{[]byte("}"), UNKNOWN},
	}

	result := parse(&input)
	// Compare array length first
	if len(result) != len(lexems) {
		t.Fatal("function parse() return array with wrong lenth\n Expected: ", len(lexems), "\nReceived: ", len(result))
	}
	// Compare values
	for i, r := range result {
		if compare(&r.Token, &lexems[i].Token) != true {
			t.Fatal("function parse() return wrong value at line: ", i, "\nExpected: ", lexems[i].Token, "\nReceived: ", r.Token)
		}
	}
	// Compare types
	for i, r := range result {
		if r.Type != lexems[i].Type {
			t.Fatal("function parse() return wrong type for: ", string(r.Token), " at line: ", i, "\nExpected: ", lexems[i].Type, "\nReceived: ", r.Type)
		}
	}
}

// Test replace(..)
func TestReplace(t *testing.T) {
	lexems := []*Lexem{
		{[]byte("async"), ASYNC},
		{[]byte("function"), FUNCTION},
		{[]byte("postRequest"), UNKNOWN},
		{[]byte("("), UNKNOWN},
		{[]byte("try"), UNKNOWN},
		{[]byte("{"), UNKNOWN},
		{[]byte("const"), CONST},
		{[]byte("response"), UNKNOWN},
		{[]byte("="), UNKNOWN},
		{[]byte("await"), AWAIT},
		{[]byte("fetch"), UNKNOWN},
		{[]byte("("), UNKNOWN},
		{[]byte("url"), UNKNOWN},
		{[]byte(")"), UNKNOWN},
		{[]byte("}"), UNKNOWN},
	}

	rep := []*Replace{
		{[]byte("postRequest"), []byte("a")},
		{[]byte("response"), []byte("b")},
	}

	final := []*Lexem{
		{[]byte("async"), ASYNC},
		{[]byte("function"), FUNCTION},
		{[]byte("a"), UNKNOWN},
		{[]byte("("), UNKNOWN},
		{[]byte("try"), UNKNOWN},
		{[]byte("{"), UNKNOWN},
		{[]byte("const"), CONST},
		{[]byte("b"), UNKNOWN},
		{[]byte("="), UNKNOWN},
		{[]byte("await"), AWAIT},
		{[]byte("fetch"), UNKNOWN},
		{[]byte("("), UNKNOWN},
		{[]byte("url"), UNKNOWN},
		{[]byte(")"), UNKNOWN},
		{[]byte("}"), UNKNOWN},
	}

	r := replace(&lexems, 1)
	// test replace table
	for i, u := range r {
		if compare(&u.Current, &rep[i].Current) != true {
			t.Fatal("replace() error in []Replace table\n Expected: ", &u.Current, "\nReceived: ", &rep[i].Current)
		}
		if compare(&u.New, &rep[i].New) != true {
			t.Fatal("replace() error in []Replace table\n Expected: ", &u.New, "\nReceived: ", &rep[i].New)
		}
	}
	// test changes in []Lexem
	for i, l := range lexems {
		if compare(&l.Token, &final[i].Token) != true {
			t.Fatal("replace() make wrong changes in []Lexem\n Expected: ", &final[i].Token, "\nReceived: ", &l.Token)
		}
	}
}

func TestReplaceImport(t *testing.T) {
	// import Car from './lib/class.js'
	// import * as mp from './lib/module.js';
	// import './lib/module2.js';
	lexems := []*Lexem{
		{[]byte("import"), IMPORT},
		{[]byte("Car"), UNKNOWN},
		{[]byte("from"), UNKNOWN},
		{[]byte("'./lib/class.js'"), STRING},
		{[]byte("import"), IMPORT},
		{[]byte("*"), UNKNOWN},
		{[]byte("as"), UNKNOWN},
		{[]byte("mp"), UNKNOWN},
		{[]byte("from"), UNKNOWN},
		{[]byte("'./lib/module.js'"), STRING},
		{[]byte(";"), UNKNOWN},
		{[]byte("import"), IMPORT},
		{[]byte("'./lib/module2.js'"), STRING},
	}

	result := replace(&lexems, 1)

	_ = result
}
