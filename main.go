package main

import (
	"flag"
	"os"
	"time"
)

type Lexem struct {
	Token []byte // token name
	Type  byte   // token type
}

type Replace struct {
	Current []byte // current token name
	New     []byte // new token name
}

type Source struct {
	path *string     // source file path
	L    *[]*Lexem   // lexems
	R    *[]*Replace // replace table
}

// Find position of '*/'
func findEndMultilineComment(input *[]byte, position int) int {
	for {
		if (*input)[position] == ASTERISK && (*input)[position+1] == SLASH {
			return position + 1
		}
		position++
	}
}

// Find position of LF
func findEndSinglelineComment(input *[]byte, position int) int {
	for {
		if (*input)[position] == LF {
			return position
		}
		position++
	}
}

// Find position of closing string token SYMBOL
func findEndString(input *[]byte, position int, SYMBOL byte) int {
	for {
		position++
		// ignore escaped end of string symbol
		if (*input)[position] == SYMBOL && (*input)[position-1] != BACKSLASH {
			return position
		}
	}
}

// Check if input symbol is breaker
// If symbol ascii number is not in one of these ranges
// digits: 48-57, capital letters: 65-90, letters: 97-122
// it is breaker
func isBreaker(symbol byte) bool {
	if (symbol > 47 && symbol < 58) ||
		(symbol > 64 && symbol < 91) ||
		(symbol > 96 && symbol < 123) {
		return false
	} else {
		return true
	}
}

// Iterate over two []byte arrays and compare its values
func compare(token *[]byte, sample *[]byte) bool {
	length := len(*token)

	// Length of token and sample not the same so they not match
	if length != len(*sample) {
		return false
	}

	// Compare each byte in loop
	for i := 0; i < length; i++ {
		if (*token)[i] != (*sample)[i] {
			return false
		}
	}
	// We reach the end of the loop, so all bytes are match
	return true
}

// Check if given sample is in KEYWORDS map
// If found return keyword type or UNKNOWN if not
func isKeyword(sample []byte) byte {
	for j, k := range KEYWORDS {
		result := compare(&sample, k)
		if result == true {
			return j
		}
	}
	return UNKNOWN
}

// Return new short name by the number
// Lower case english letters a-z = 97-122
// Capital letters A-Z = 65-90
func newName(number int) *[]byte {
	result := []byte{}
	// For some input numbers we can get as result JavaScript keyword
	// For names generated for this numbers we add additional 'a' symbol
	// at the beginning to distiquish from JavaScript keyword
	//
	// For JavaScript keywords larger than 3 letters there are no such check
	// If in your project there are more than 140608 variables, you need to add support by yourself
	//
	// Its ugly but its faster than iterating over keyword codes in loop
	switch number {
	case 321: // if
		return &[]byte{97, 105, 102}
	case 784: // do
		return &[]byte{97, 100, 111}
	case 737: // in
		return &[]byte{97, 105, 110}
	case 54352: // let
		return &[]byte{97, 108, 101, 116}
	case 62466: // new
		return &[]byte{97, 110, 101, 119}
	case 48746: // var
		return &[]byte{97, 118, 97, 114}
	case 49458: // for
		return &[]byte{97, 102, 111, 114}
	case 68556: // try
		return &[]byte{97, 116, 114, 121}
	}

	// Divide by 52 and save reminder as letter code
	for {
		quotient := number / 52
		reminder := number % 52

		if reminder > 25 {
			// capital letters
			result = append(result, byte(reminder+38))
		} else {
			// lowercase letters
			result = append(result, byte(reminder+96))
		}

		if quotient > 0 {
			number = quotient
		} else {
			break
		}
	}
	return &result
}

// Compares each byte in input byte array with 'breaker' symbol
// If found one split string on this symbol and saves all bytes from
// previous 'breaker' position to curret as 'token' in []*Lexem
// Omits comments, spaces and empty lines
func parse(input *[]byte) []*Lexem {
	prev := 0
	lex := []*Lexem{}
	length := len(*input)

	// Split text to Tokens
	for i := 0; i < length; i++ {
		if isBreaker((*input)[i]) == true {
			// ignore comment and shift loop index to the comment last byte position
			// eq. '//'
			if (*input)[i] == SLASH && (*input)[i+1] == SLASH {
				i = findEndSinglelineComment(input, i)
				prev = i + 1
				continue
			}
			// eq. '/*'
			if (*input)[i] == SLASH && (*input)[i+1] == ASTERISK {
				i = findEndMultilineComment(input, i)
				prev = i + 1
				continue
			}
			// tread string in single quote
			if (*input)[i] == APOSTROPHE {
				i = findEndString(input, i, APOSTROPHE)
				lex = append(lex, &Lexem{(*input)[prev : i+1], STRING})
				prev = i + 1
				continue
			}
			// tread string in double quote
			if (*input)[i] == QUOTATION {
				i = findEndString(input, i, QUOTATION)
				lex = append(lex, &Lexem{(*input)[prev : i+1], STRING})
				prev = i + 1
				continue
			}
			// If previous symbols are not breakers
			// This mean its a keyword or function or variable name
			if prev < i {
				result := isKeyword((*input)[prev:i])
				switch result {
				case UNKNOWN:
					lex = append(lex, &Lexem{(*input)[prev:i], result})
				case FUNCTION:
					if (*input)[i] != SPACE {
						lex = append(lex, &Lexem{(*input)[prev:i], result})
					} else {
						lex = append(lex, &Lexem{(*input)[prev : i+1], result})
					}
				case OF:
					lex = append(lex, &Lexem{(*input)[prev-1 : i+1], result})
				default:
					lex = append(lex, &Lexem{(*input)[prev : i+1], result})
				}
			}
			// automatic semicolon insertion in dumb way
			// SEMICOLON := []byte{59}
			// todo: try to do it with token types like import
			if ((*input)[i] == LF && (*input)[i-1] == BRACKET_CLOSED) ||
				((*input)[i] == LF && (*input)[i-1] == CURLYBRACKET_CLOSED) ||
				((*input)[i] == LF && (*input)[i-1] == QUOTATION) ||
				((*input)[i] == LF && (*input)[i-1] == APOSTROPHE) {
				lex = append(lex, &Lexem{[]byte{59}, UNKNOWN})
			}
			// Skip space, tab and empty new line
			if (*input)[i] == SPACE || (*input)[i] == TAB ||
				(*input)[i] == LF || (*input)[i] == CR {
				prev = i + 1
				continue
			}
			// Save current breaker
			lex = append(lex, &Lexem{[]byte{(*input)[i]}, UNKNOWN})
			prev = i + 1
		}
	}
	return lex
}

// Iterate over []Lexem
// Search for functions and vaiables names
// Store it in []Replace replacement table
// And generate new short names
func prepare(lexems *[]*Lexem, start int) []*Replace {
	replace := []*Replace{}
	size := len(*lexems)

	for i := 0; i < size; i++ {
		if (*lexems)[i].Type == IMPORT {
			for {
				// iterate over tokens until find token with type STRING
				// this token is file name to import
				// add it to import slice
				// replace all tokens from import expression with empty bytes
				if (*lexems)[i].Type == STRING {
					// todo: add to import structure
					println(string((*lexems)[i].Token))

					(*lexems)[i].Token = []byte{}
					if i+i <= size && (*lexems)[i+1].Token[0] == SEMICOLON {
						(*lexems)[i+1].Token = []byte{}
					}
					break
				}
				(*lexems)[i].Token = []byte{}
				i++
			}
		}
		if (*lexems)[i].Type == FUNCTION {
			// If next token is bracket '(' function does not has name
			if (*lexems)[i+1].Token[0] != BRACKET_OPEN {
				replace = append(replace, &Replace{(*lexems)[i+1].Token, *newName(start)})
				start++
			}
		}
		if (*lexems)[i].Type == VAR || (*lexems)[i].Type == CONST || (*lexems)[i].Type == LET {
			replace = append(replace, &Replace{(*lexems)[i+1].Token, *newName(start)})
			start++
		}
	}
	return replace
}

// Replace tokens in []Lexem with short names
func replace(source *[]*Source) {
	for _, s := range *source {
		size := len(*s.L)
		// Nested loop :(
		for i := 0; i < size; i++ {
			for _, r := range *s.R {
				if compare(&(*s.L)[i].Token, &r.Current) == true {
					(*s.L)[i].Token = r.New
				}
			}
		}
	}
}

// Save []Lexems to output file
func save(source *[]*Source, output *string) {
	// Don't care if output file exist owerride it
	o, err := os.Create(*output)
	if err != nil {
		println(err)
		os.Exit(1)
	}
	defer o.Close()

	for _, g := range *source {
		for _, l := range *g.L {
			o.Write(l.Token)
		}
	}
}

// Open input file and read it content to []byte buffer
func open(input *string) *[]byte {
	_, err := os.Stat(*input)
	if err != nil {
		println(err)
		os.Exit(1)
	}

	buffer, err := os.ReadFile(*input)
	if err != nil {
		println(err)
		os.Exit(1)
	}

	return &buffer
}

// Cheap alternative to wait group
var count int = 0
var mutex bool = false

func lock() {
	for {
		if mutex == true {
			time.Sleep(10)
		} else {
			mutex = true
			break
		}
	}
}
func unlock() {
	mutex = false
}

// run 'in' source file processing in thread
// return result to 'out *[]*Source'
func runner(in *string, out *[]*Source) {
	// add to thread counts
	count++

	lock()
	// check if file already parsed
	for _, f := range *out {
		if f.path == in {
			unlock()
			return
		}
	}
	// save item array index and add new source to array
	idx := len(*out)
	*out = append(*out, &Source{in, nil, nil})
	unlock()

	// process
	b := open(in)
	l := parse(b)
	r := prepare(&l, 1)

	// save result
	(*out)[idx].L = &l
	(*out)[idx].R = &r

	// thread are done, remove it from thread count
	count--
}

func main() {
	input := flag.String("i", "", "Input script file path")
	output := flag.String("o", "", "Output result file")
	flag.Parse()

	out := []*Source{}

	// todo: unsyncronized with main thread
	go runner(input, &out)

	// in main thread wait until all threads are done
	for {
		if count != 0 {
			time.Sleep(100)
		} else {
			break
		}
	}

	replace(&out)
	save(&out, output)
}
