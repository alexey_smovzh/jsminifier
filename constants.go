// ECMAScript 5 and 6 compliant
package main

// Special symbols
const BRACKET_OPEN = byte(40)         // (
const BRACKET_CLOSED = byte(41)       // )
const CURLYBRACKET_CLOSED = byte(125) // }
const SLASH = byte(47)                // /
const BACKSLASH = byte(92)            // \
const ASTERISK = byte(42)             // *
const QUOTATION = byte(34)            // "
const APOSTROPHE = byte(39)           // '
const SEMICOLON = byte(59)            // ;
const TAB = byte(9)
const SPACE = byte(32)
const LF = byte(10)
const CR = byte(13)

// Keywords codes
const IMPORT = 0
const CLASS = 1
const FUNCTION = 2
const CONST = 3
const VAR = 4
const LET = 5
const ASYNC = 6
const AWAIT = 7
const RETURN = 8
const CASE = 9
const OF = 10
const NEW = 11
const STRING = 249
const UNKNOWN = 250

// Keywords
var KEYWORDS = map[byte]*[]byte{
	IMPORT:   {105, 109, 112, 111, 114, 116},
	CLASS:    {99, 108, 97, 115, 115},
	FUNCTION: {102, 117, 110, 99, 116, 105, 111, 110},
	CONST:    {99, 111, 110, 115, 116},
	VAR:      {118, 97, 114},
	LET:      {108, 101, 116},
	ASYNC:    {97, 115, 121, 110, 99},
	AWAIT:    {97, 119, 97, 105, 116},
	RETURN:   {114, 101, 116, 117, 114, 110},
	CASE:     {99, 97, 115, 101},
	OF:       {111, 102},
	NEW:      {110, 101, 119},
}

/*
abstract	arguments	await*	        boolean
		    		    		        continue
debugger	default	    delete	        do
double	    		    enum*	        eval
export*	    extends*			        final
finally	    		    for
goto	    	        implements
in	        instanceof
		    		    native	        new
null	    package	    private	        protected
public	    		    		        static
super*	    		    synchronized
throw	    throws	    transient
	        typeof	    	            void
volatile	while	    with	        yield
*/
